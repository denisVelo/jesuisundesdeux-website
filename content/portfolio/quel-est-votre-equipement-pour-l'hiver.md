---
title: "Quel est votre équipement pour l'hiver ?"
type: portfolio
date: 2018-11-23T13:11:00
submitDate: 23 Novembre 2018
caption: Aide citoyenne
image: images/portfolio/hiver.jpg
category: ["conseil","aide"]
client: Paul Leclerc
projectname: Dicussion sur Framasoft
projectlink: https://framavox.org/d/CgOlbis6/quel-est-votre-quipement-pour-l-hiver-

---
### Quel est votre équipement pour l'hiver ?

Pour se partager nos bons plans en terme de protection et de visibilité

Lumière (30€):

2 à l'arrière en mode clignotant https://www.decathlon.fr/vioo-clip-100-arriere-usb-id_8396511.html
1 avant sur le casque : https://www.decathlon.fr/frontale-trail-onnight-410-id_8503711.html
Je vais retester des lampes qui envoient des laser sur le côté, j'avais vu une vraie différence avec l'écart des voitures:

https://fr.aliexpress.com/item/FTW-Queue-De-Bicyclette-Lumi-re-USB-Rechargeable-10-Ligne-Laser-V-lo-Arri-re-Lumi/32885589731.html
Ajouté sur mon cadre et sur mon casque (1€): stoch refléchissant

https://www.amazon.fr/Intensit%C3%A9-Autocollant-R%C3%A9fl%C3%A9chissant-Vinyle-Auto-adh%C3%A9sif/dp/B01AN0NE1S/ref=sr_1_sc_1?ie=UTF8&qid=1542974714&sr=8-1-spell&keywords=sctoch+refl%C3%A9chissant
Sonnette (4€):

https://www.lecyclo.com/velo/securite/klaxon-sonnette/sonnette-pour-velo.html
Casque avec visière (pluie/soleil) et cache oreilles (26€):

https://www.decathlon.fr/casque-velo-bol-500-bleu-fonce-id_8402992.html
Gants si il fait pas trop froid (5€):

https://www.decathlon.fr/gants-velo-roadc-100-id_8327103.html sinon gants en laine
J'ai une veste coupe vent/imperméable de bateau que j'ai recyclé pour le vélo
