---
title: "Cartographier les pistes cyclables"
type: portfolio
date: 2018-11-11T12:51:00
submitDate: 11 Novembre 2018
caption: Aide citoyenne
image: images/portfolio/map.png
category: ["action","aide"]
client: Brice Frabre, Denis Feurer, Bruno Adelé
projectname: Dicussion sur Framasoft
projectlink: https://framavox.org/d/5dQzN3yV/cartographier-les-pistes-cyclables-sur-openstreetmap

---
### Cartographier les pistes cyclables

L'idée est de cartographier les pistes cyclables entre midi et 2 ou le week-end et de finir par un pique nique :)

Le but est de :
- Recenser l'état actuel des pistes cyclabes et actualiser l'état sur [OpenStreetMap](https://wiki.openstreetmap.org/wiki/FR:Bicycle), par la même occasion identifier les problèmes :

- linéaires (voie cyclable sur trottoir qui prend la place des piétons, voie avec un arbre tous les mètres)
- ponctuels permanents (sortie de voie cyclable hasardeuse, problème sur la voie)
- ponctuels "temporaires" (#GCUM et autres mauvais usages des voies cyclables). Sur ce sujet il faut être raccord avec tout ce qui existe dont http://www.velocite-montpellier.fr/velobs/

Pour cartographier toutes ces informations, il y a déjà des éléments dans la structure de données OSM, comme par exemple la propriété "segregated" et autres sur les "cycleway". Il faudrait que nous consultions https://wiki.openstreetmap.org/wiki/Key:cycleway et la communauté OSM MTP pour avoir des recommandations de bonnes pratiques de carto de voies cyclables.