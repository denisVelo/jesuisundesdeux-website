---
title: "Hotspot"
type: portfolio
date: 2018-11-16T11:57:54
submitDate: 16 Novembre 2018
caption: Aide citoyenne
image: images/portfolio/hotspot.jpg
category: ["action","aide"]
client: Brice Frabre, Bruno Adelé
projectname: Dicussion sur Framasoft
projectlink: https://framavox.org/d/vmrUJFD3/discussion-sur-les-hotspots

---
### Hotspot

Voici un guide à destination des gens voulant créer et animer un hotspot.

#### Identifier et créer un hotspot

Vous prenez d'un parcours régulier. La première étape consiste à renseigner votre trajet sur la [carte suivante je veux velotafer](https://umap.openstreetmap.fr/en/map/je-veux-velotafer_266810#14/43.6032/3.8965)

Les recoupements permettent d'identifier les zones où le plus de cyclistes sont susceptibles de se rencontrer. Pour plus de facilité, nous vous proposons d'utiliser les codes suivant sur google map : https://plus.codes/map/. Ces codes sont réutilisables sur Google Map.

#### Qu'est ce qu'un bon hostpot?

Un bon hostpot c'est un lieu où on peut se parquer facilement et se retrouver, idéalement placé pas loin de quelques pistes cyclables.

#### Se donner rendez-vous

Une fois que vous avez identifié votre ou vos hotspots, indiquez quand vous pouvez vous y trouver, que ce soit sur les réseaux sociaux ou sur le framavox. Prévoyez d'y rester sur une plage de temps raisonnable.

#### Vélotafer ensemble

Une fois que vous vous êtes retrouvés, c'est à vous de rouler ensemble et en toute sécurité. Vous pouvez vous rendre vers un autre hotspot ou bien rouler plus longuement vers vos destinations. L'objectif est de se rencontrer, de papoter et, bien sûr, d'arriver en tout sécurité.
